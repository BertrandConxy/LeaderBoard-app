 ![](https://img.shields.io/badge/Microverse-blueviolet)
# LeaderBoard-app
Have you come across any leaderboard, like billboard for example where people are ranked according to their performance. This leaderboard app is also like that, It ranks the people according to their scores in a game.

## Built With

- HTML and CSS
- plain Javascript

## How to use and run this project

> To get a local copy up and running follow these simple example steps.

-Clone this repository with

<code> git clone <https://github.com/BertrandConxy/LeaderBoard-app.git> </code>

-using your terminal or command line.

-Change to the project directory by entering :

<code>cd LeaderBoard-app</code>, in the terminal.

-run code to open it in vscode.
However, there are some project requisites required for this project to run properly

### Prerequisites

- This project makes use of bundler called 'Webpack' which manages all the dependencies and files for this project.
- To install it, follow this:
- Locate to the directory of the project
- In the terminal, <code>npm init -y</code> to initialise the package manager
- Again, in the terminal, <code> npm install </code> to install everything.
  - to run the development code <code> npm run build </code>

## Project Status

This project has been finished and deployed.

## Screenshoots

![Screenshot (18)](https://user-images.githubusercontent.com/90222110/152327869-1effb9d6-f088-4bf3-a920-5f6cee7b6ec9.png)



## Live Demo link

<https://bertrandconxy.github.io/LeaderBoard-app/>

## Issues

Up to now, there are no issues with it.

Here is the link to the Issues tab:

<https://github.com/BertrandConxy/LeaderBoard-app/issues>

## Authors

👤 **Bertrand Mutangana Ishimwe**

- GitHub: [@BertrandConxy](https://github.com/BertrandConxy)
- Twitter: [@Bconxy](https://twitter.com/Bconxy)
- LinkedIn: [Bertrand Mutangana Ishimwe](https://www.linkedin.com/in/bertrand-mutangana-024905220/)


## Show your support

Give a ⭐️ if you like this project!

## Acknowledgments
- Thanks  Kandi for your help
- Thanks  Abood for your help
- Thanks to everyone else who helped me.
